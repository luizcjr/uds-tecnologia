package com.uds.udstecnologia.di

import com.uds.udstecnologia.ui.base.BaseActivity
import dagger.Component

@Component(modules = [FirebaseAuthenticationModule::class, FirebaseDatabaseModule::class])
interface ActivityComponent {
    fun inject(activity: BaseActivity)
}