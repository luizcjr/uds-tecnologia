package com.uds.udstecnologia.di

import com.uds.udstecnologia.ui.base.BaseViewModel
import dagger.Component

@Component(modules = [FirebaseAuthenticationModule::class, FirebaseDatabaseModule::class])
interface ViewModelComponent {
    fun inject(viewModel: BaseViewModel)
}