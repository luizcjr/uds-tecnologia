package com.uds.udstecnologia.ui.interfaces

interface GuidelinesListener {
    fun action(position: Int)
}