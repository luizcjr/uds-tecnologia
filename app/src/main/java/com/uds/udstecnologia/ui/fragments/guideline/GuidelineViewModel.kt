package com.uds.udstecnologia.ui.fragments.guideline

import androidx.lifecycle.MutableLiveData
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.uds.udstecnologia.models.GuidelineModelAdapter
import com.uds.udstecnologia.models.User
import com.uds.udstecnologia.ui.activities.include_guideline.IncludeGuidelineActivity
import com.uds.udstecnologia.ui.activities.login.LoginActivity
import com.uds.udstecnologia.ui.base.BaseViewModel
import com.uds.udstecnologia.util.Util.openActivity

class GuidelineViewModel : BaseViewModel() {

    var response = MutableLiveData<User>()

    var idUser = ""
    var nameUser = ""
    var emailUser = ""
    var passwordUser = ""

    private lateinit var mDatabaseReferenceGuideline: DatabaseReference
    var updateResponse = MutableLiveData<String>()

    val guidelinesOpen by lazy { MutableLiveData<ArrayList<GuidelineModelAdapter>>() }
    val guidelinesFinish by lazy { MutableLiveData<ArrayList<GuidelineModelAdapter>>() }

    fun getAllGuidelines() {
        loading.value = true

        mDatabaseReference = mDatabase.reference.child("users")
        mDatabaseReferenceGuideline = mDatabase.reference.child("guidelines")

        val mUser = firebaseAuth.currentUser!!

        val guidelinesListOpen = ArrayList<GuidelineModelAdapter>()
        val guidelinesListFinish = ArrayList<GuidelineModelAdapter>()

        mDatabaseReferenceGuideline.child(mUser.uid)
            .addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    loadError.value = "Falha ao cadastrar pauta. Tente novamente mais tarde!"
                    loading.value = false
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    loadError.value = null
                    loading.value = false

                    guidelinesOpen.value = null
                    guidelinesFinish.value = null

                    for (i in snapshot.children) {
                        val title = i.child("title").value as String
                        val descriptionShort = i.child("descriptionShort").value as String
                        val description = i.child("description").value as String
                        val author = i.child("author").value as String
                        val isOpen = i.child("open").value as Boolean
                        val id = i.child("id").value as String

                        if (isOpen) {
                            guidelinesListOpen.add(
                                GuidelineModelAdapter(
                                    title, descriptionShort, description, author, false, isOpen, id
                                )
                            )

                            guidelinesOpen.value = guidelinesListOpen
                        } else {
                            guidelinesListFinish.add(
                                GuidelineModelAdapter(
                                    title, descriptionShort, description, author, false, isOpen, id
                                )
                            )

                            guidelinesFinish.value = guidelinesListFinish
                        }
                    }
                }
            })
    }

    fun clear() {
        guidelinesOpen.value = arrayListOf()
        guidelinesFinish.value = arrayListOf()
    }

    fun updateGuideline(
        action: Boolean,
        title: String,
        descriptionShort: String,
        description: String,
        author: String,
        id: String
    ): MutableLiveData<String> {
        updateResponse = MutableLiveData()

        //Criando instancia do firebase
        mDatabaseReference = mDatabase.reference.child("users")
        mDatabaseReferenceGuideline = mDatabase.reference.child("guidelines")

        val mUser = firebaseAuth.currentUser!!

        mDatabaseReferenceGuideline.child(mUser.uid).child(id)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    updateResponse.value = null

                    loadError.value = "Falha ao cadastrar pauta. Tente novamente mais tarde!"
                    loading.value = false
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    snapshot.ref.child("open").setValue(action)
                    snapshot.ref.child("title").setValue(title)
                    snapshot.ref.child("descriptionShort").setValue(descriptionShort)
                    snapshot.ref.child("description").setValue(description)
                    snapshot.ref.child("author").setValue(author)

                    updateResponse.value = "Sucesso"

                    loadError.value = null
                    loading.value = false
                }
            })

        return updateResponse
    }

    fun getUserInfo(): MutableLiveData<User> {
        loading.value = true

        mDatabaseReference = mDatabase.reference.child("users")

        val mUser = firebaseAuth.currentUser!!
        val mUserReference = mDatabaseReference.child(mUser.uid)

        mUserReference.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                loadError.value = "Falha ao recuperar dados do usuário. Tente novamente mais tarde!"
                loading.value = false
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                loadError.value = null
                loading.value = false

                idUser = snapshot.child("id").value as String
                nameUser = snapshot.child("name").value as String
                emailUser = snapshot.child("email").value as String
                passwordUser = snapshot.child("password").value as String

                response.value = User(idUser, nameUser, emailUser, passwordUser)
            }
        })

        return response
    }

    fun logOut() {
        firebaseAuth.signOut()

        context.openActivity<LoginActivity>()
    }

    fun includeGuidelines() {
        context.openActivity<IncludeGuidelineActivity>() {
            putExtra("name", nameUser)
        }
    }
}