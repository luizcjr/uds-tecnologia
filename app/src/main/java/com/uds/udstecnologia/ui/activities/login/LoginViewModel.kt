package com.uds.udstecnologia.ui.activities.login

import android.content.Intent
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.mlykotom.valifi.ValiFiForm
import com.mlykotom.valifi.fields.ValiFieldEmail
import com.mlykotom.valifi.fields.ValiFieldText
import com.uds.udstecnologia.ui.activities.forgot_password.ForgotPasswordActivity
import com.uds.udstecnologia.ui.activities.main.MainActivity
import com.uds.udstecnologia.ui.activities.register.RegisterActivity
import com.uds.udstecnologia.ui.base.BaseViewModel
import com.uds.udstecnologia.util.Util.openActivity


class LoginViewModel : BaseViewModel() {

    val email = ValiFieldEmail().addEmailValidator("E-mail inválido!")
    val password =
        ValiFieldText().addMinLengthValidator("Senha curta!", 6)

    val form = ValiFiForm(email, password)

    //Evento do clique do botão de logar
    fun sendLogin() {
        loading.value = true

        firebaseAuth.signInWithEmailAndPassword(email.value.toString(), password.value.toString())
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    loadError.value = null
                    loading.value = false

                    val user = firebaseAuth.currentUser
                    firebaseUser.value = user
                } else {
                    loadError.value = "E-mail e/ou senha incorretos!"
                    loading.value = false

                    firebaseUser.value = null
                }
            }
    }

    //Função para encaminhar para o cadastro
    fun register() {
        context.openActivity<RegisterActivity>()
    }

    //Função para encaminhar para a recuperação de senha
    fun forgotPassword() {
        context.openActivity<ForgotPasswordActivity>()
    }


}