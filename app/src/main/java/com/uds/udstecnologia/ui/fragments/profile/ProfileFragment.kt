package com.uds.udstecnologia.ui.fragments.profile

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.uds.udstecnologia.R
import com.uds.udstecnologia.databinding.ProfileBinding
import com.uds.udstecnologia.models.User
import com.uds.udstecnologia.ui.base.BaseFragment
import com.uds.udstecnologia.ui.custom.AlertDefault

class ProfileFragment : BaseFragment() {

    private lateinit var viewModel: ProfileViewModel
    private var binding: ProfileBinding? = null

    private val userDataObserver = Observer<User> { user ->
        user.let {
            getParentActivity()!!.title = user.name
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)

        return binding!!.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ProfileViewModel::class.java)

        setupToolbar()
        setHasOptionsMenu(true)

        binding!!.profilleViewModel = viewModel
        binding!!.lifecycleOwner = this

        viewModel.loading.observe(viewLifecycleOwner, loadingLiveDataObserver)
        viewModel.loadError.observe(viewLifecycleOwner, errorLiveDataObserver)
        viewModel.loadSuccess.observe(viewLifecycleOwner, successLiveDataObserver)
        viewModel.context = requireContext()
        viewModel.getUserInfo().observe(viewLifecycleOwner, userDataObserver)
    }

    private fun setupToolbar() {
        getParentActivity()!!.setSupportActionBar(binding!!.toolbar)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.toolbar_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.exit) {
            showDialogExit()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun showDialogExit() {
        val alertDefault = AlertDefault(
            requireContext(),
            "Atenção",
            "Deseja realmente sair?",
            true
        )

        alertDefault.addButton("Não", R.style.ButtonOutline, View.OnClickListener {
            alertDefault.dismiss()
        })

        alertDefault.addButton("Sim", R.style.ButtonDefault, View.OnClickListener {
            viewModel.logOut()
            alertDefault.dismiss()
        })
        alertDefault.show()
    }
}