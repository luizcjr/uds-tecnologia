package com.uds.udstecnologia.ui.activities.include_guideline

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.uds.udstecnologia.R
import com.uds.udstecnologia.databinding.IncludeGuidelineBinding
import com.uds.udstecnologia.ui.base.BaseActivity
import kotlinx.android.synthetic.main.toolbar.view.*


class IncludeGuidelineActivity : BaseActivity() {

    private lateinit var viewModel: IncludeGuidelineViewModel
    private var binding: IncludeGuidelineBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // data binding class
        binding =
            DataBindingUtil.setContentView(this, R.layout.activity_include_guideline)

        //ViewModel
        viewModel = ViewModelProviders.of(this)[IncludeGuidelineViewModel::class.java]

        binding!!.toolbar.toolbarTitle.text = getString(R.string.title_include_guideline)
        binding!!.toolbar.toolbar.setNavigationOnClickListener { onBackPressed() }

        val name = intent.getStringExtra("name")
        name.let {
            viewModel.author.value = name
        }

        binding!!.includeGuidelineViewModel = viewModel
        binding!!.lifecycleOwner = this

        viewModel.loading.observe(this, loadingLiveDataObserver)
        viewModel.loadError.observe(this, errorLiveDataObserver)
        viewModel.context = this
    }
}
