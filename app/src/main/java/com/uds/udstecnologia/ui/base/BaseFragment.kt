package com.uds.udstecnologia.ui.base

import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.uds.udstecnologia.models.User
import com.uds.udstecnologia.ui.activities.main.MainActivity
import com.uds.udstecnologia.util.Util

abstract class BaseFragment : Fragment() {
    protected val loadingLiveDataObserver = Observer<Boolean> { isLoading ->
        if (isLoading) {
            Util.onStartLoading(requireContext())
        } else {
            Util.onStopLoading()
        }
    }

    protected val errorLiveDataObserver = Observer<String> { error ->
        if (error != null) {
            Util.alertError(error, requireContext())
        }
    }

    protected val userLiveDataObserver = Observer<User> { user ->
        user.let {
            getParentActivity()!!.title = user.name
        }
    }

    protected val successLiveDataObserver = Observer<String> { success ->
        if (success != null) {
            Util.alertSuccess(success, requireContext())
        }
    }

    protected fun getParentActivity(): MainActivity? {
        return activity as MainActivity?
    }
}