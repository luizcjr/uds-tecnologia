package com.uds.udstecnologia.ui.activities.splash_screen

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.uds.udstecnologia.R
import com.uds.udstecnologia.ui.activities.login.LoginActivity

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        //Contador para que a splash abra a activity de login
        val handler = Handler()
        handler.postDelayed({
            startActivity(LoginActivity::class.java)
        }, 2500)
    }

    //Função para abrir a activity de login
    private fun startActivity(redirect: Class<*>) {
        startActivity(Intent(this@SplashScreenActivity, redirect))
    }

    //Quando o botão de retornar físico do aparelho é apertado, nada acontece para não atrapalhar a transição de tela
    override fun onBackPressed() {

    }
}
