package com.uds.udstecnologia

import android.app.Application
import android.content.Context
import com.mlykotom.valifi.ValiFi

public open class App : Application() {

    companion object {
        var appContext: Context? = null
            private set
    }

    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext

        ValiFi.install(this)
    }
}