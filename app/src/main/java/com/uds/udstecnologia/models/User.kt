package com.uds.udstecnologia.models

class User(val id: String, val name: String, val email: String, val password: String)