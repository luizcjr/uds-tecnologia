package com.uds.udstecnologia.models

class GuidelineModelAdapter(
    var title: String = "",
    val descriptionShort: String = "",
    val description: String = "",
    val author: String = "",
    var expanded: Boolean = false,
    var isOpen: Boolean = false,
    val id: String = ""
){}